# Copyright 2015-2023 Timo Gurr <tgurr@exherbo.org>
# Distributed under the terms of the GNU General Public License v2
# Based in part upon 'kodi-9999.ebuild' from Gentoo, which is:
#     Copyright 1999-2015 Gentoo Foundation

require github [ user=xbmc pn=xbmc ] \
    cmake \
    freedesktop-desktop \
    gtk-icon-cache \
    python [ blacklist=2 multibuild=false has_bin=true python_opts=[sqlite] ]

if ever is_scm ; then
    require ffmpeg [ abis=[ 5 6 ] options="[h264]" ]
else
    require ffmpeg [ abis=[ 4 ] min_versions=[ 4.4.1 ] options="[h264]" ]
fi

export_exlib_phases src_prepare src_configure pkg_postinst pkg_postrm

SUMMARY="Kodi is an award-winning free and open source software media center"
HOMEPAGE="https://${PN}.tv"

DOWNLOADS+="
    https://github.com/xbmc/libdvdcss/archive/${LIBDVDCSS_REV}.tar.gz -> libdvdcss-${LIBDVDCSS_REV}.tar.gz
    https://github.com/xbmc/libdvdread/archive/${LIBDVDREAD_REV}.tar.gz -> libdvdread-${LIBDVDREAD_REV}.tar.gz
    https://github.com/xbmc/libdvdnav/archive/${LIBDVDNAV_REV}.tar.gz -> libdvdnav-${LIBDVDNAV_REV}.tar.gz
"

LICENCES="GPL-2"
SLOT="0"
MYOPTIONS="
    airplay [[ description = [ Enable support for Airplay, wireless local media streaming ] ]]
    alsa
    avahi
    bluetooth
    nfs [[ description = [ Access remote media via NFS ] ]]
    pulseaudio
    samba [[ description = [ Access remote media via samba ] ]]
    sync [[ description = [ Enable syncing multiple Kodi installations via MySQL ] ]]
    va [[ description = [ Enable hardware Video Acceleration (VA API) ] ]]
    vdpau [[ description = [ Enable hardware Video Acceleration (VDPAU) for NVidia GPUs ] ]]

    (
        X
        wayland
        gbm
    ) [[ number-selected = exactly-one ]]
    ( providers: eudev systemd ) [[ number-selected = exactly-one ]]
    ( providers: ijg-jpeg jpeg-turbo ) [[ number-selected = exactly-one ]]
    ( providers: libressl openssl ) [[ number-selected = exactly-one ]]
"

# Fails a bunch of tests, last checked: 19.4
# 94% tests passed, 36 tests failed out of 586
RESTRICT="test"

DEPENDENCIES="
    build:
        app-doc/doxygen
        dev-lang/swig[>=2.0]
        dev-util/gperf
        virtual/jre:=[>=1.6]
    build+run:
        app-arch/lzo:2
        app-arch/unzip
        app-arch/zip
        dev-db/sqlite:3
        dev-libs/crossguid[>=0.2.2_p20190529] [[ note = [ First version shipping crossguid.pc ] ]]
        dev-libs/expat
        dev-libs/flatbuffers[>=1.9.0]
        dev-libs/fmt[>=6.1.2]
        dev-libs/fribidi
        dev-libs/fstrcmp
        dev-libs/libcdio[>=2.1.0]
        dev-libs/libglvnd[X?]
        dev-libs/libxml2:2.0
        dev-libs/libxslt
        dev-libs/pcre
        dev-libs/rapidjson[>=1.0.2]
        dev-libs/spdlog[>=1.5.0]
        dev-libs/tinyxml[>=2.6.2]
        dev-python/Pillow[>=7.1.2][python_abis:*(-)?]
        fonts/dejavu
        media/pipewire[>=0.3.24]
        media-libs/dav1d:=
        media-libs/freetype:2
        media-libs/giflib:=[>=4.1.6] [[ note = [ for TexturePacker ] ]]
        media-libs/lcms2[>=2.10]
        media-libs/libass[>=0.9.8]
        media-libs/libpng:= [[ note = [ for TexturePacker ] ]]
        media-libs/libudfread[>=1.0.0]
        media-libs/taglib[>=1.9.0]
        media-libs/tiff:= [[ note = [ for TexturePacker ] ]]
        media-sound/lame
        net-libs/libmicrohttpd[>=0.9.40]
        net-misc/curl
        sys-apps/dbus
        sys-apps/util-linux [[ note = [ libuuid ] ]]
        sys-libs/libcap
        sys-libs/zlib
        x11-dri/glu
        x11-dri/libdrm[>=2.4.95]
        x11-libs/harfbuzz
        X? (
            x11-dri/mesa [[ note = [ provides EGL/eglextchromium.h ] ]]
            x11-libs/libX11
            x11-libs/libXext
            x11-libs/libXmu
            x11-libs/libXrandr
            x11-libs/libXt
        )
        airplay? ( dev-libs/libplist:2.0 )
        alsa? ( sys-sound/alsa-lib[>=1.0.27] )
        avahi? ( net-dns/avahi )
        bluetooth? ( net-wireless/bluez )
        gbm? ( x11-dri/mesa[?X] )
        nfs? ( net-fs/libnfs[>=3.0.0] )
        pulseaudio? ( media-sound/pulseaudio[>=11.0.0] )
        samba? ( net-fs/samba )
        sync? ( virtual/mysql )
        va? ( x11-libs/libva[>=0.39.0] )
        vdpau? ( x11-libs/libvdpau )
        wayland? (
            sys-libs/wayland-protocols[>=1.7]
            sys-libs/waylandpp[>=0.2.2]
            x11-libs/libxkbcommon[>=0.4.1]
        )
        providers:eudev? ( sys-apps/eudev )
        providers:ijg-jpeg? ( media-libs/jpeg:= )
        providers:jpeg-turbo? ( media-libs/libjpeg-turbo )
        providers:libressl? ( dev-libs/libressl:= )
        providers:openssl? ( dev-libs/openssl:=[>=1.1.0] )
        providers:systemd? ( sys-apps/systemd )
    test:
        dev-cpp/gtest[>=1.10.0]
    suggestion:
        media-plugins/kodi-pvr-hts [[
            description = [ Kodi PVR client Tvheadend HTSP ]
        ]]
        media-plugins/kodi-pvr-iptvsimple [[
            description = [ Kodi PVR client IPTV Simple ]
        ]]
        media-plugins/kodi-pvr-vdr-vnsi [[
            description = [ Kodi PVR client VDR VNSI ]
        ]]
        sys-apps/udisks:2 [[
            description = [ Removable media support ]
        ]]
"

CMAKE_SRC_CONFIGURE_PARAMS=(
    -DCMAKE_FIND_ROOT_PATH:PATH="$(ffmpeg_alternatives_prefix);/usr/$(exhost --target)"
    -DADDONS_CONFIGURE_AT_STARTUP:BOOL=TRUE
    -DBUILD_SHARED_LIBS:BOOL=TRUE
    -DCMAKE_DISABLE_FIND_PACKAGE_Git:BOOL=TRUE
    -DCMAKE_INSTALL_DOCDIR:PATH=/usr/share/doc/${PNVR}
    -DENABLE_BLURAY:BOOL=FALSE
    -DENABLE_CAP:BOOL=TRUE
    -DENABLE_CCACHE:BOOL=FALSE
    -DENABLE_CEC:BOOL=FALSE
    -DENABLE_CLANGTIDY:BOOL=FALSE
    -DENABLE_CPPCHECK:BOOL=FALSE
    -DENABLE_DBUS:BOOL=TRUE
    -DENABLE_DVDCSS:BOOL=TRUE
    -DENABLE_EVENTCLIENTS:BOOL=FALSE
    -DENABLE_INCLUDEWHATYOUUSE:BOOL=FALSE
    -DENABLE_INTERNAL_CROSSGUID:BOOL=FALSE
    -DENABLE_INTERNAL_DAV1D:BOOL=FALSE
    -DENABLE_INTERNAL_FFMPEG:BOOL=FALSE
    -DENABLE_INTERNAL_FLATBUFFERS:BOOL=FALSE
    -DENABLE_INTERNAL_FMT:BOOL=FALSE
    -DENABLE_INTERNAL_FSTRCMP:BOOL=FALSE
    -DENABLE_INTERNAL_GTEST:BOOL=FALSE
    -DENABLE_INTERNAL_KISSFFT:BOOL=TRUE
    -DENABLE_INTERNAL_NFS:BOOL=FALSE
    -DENABLE_INTERNAL_PCRE:BOOL=FALSE
    -DENABLE_INTERNAL_RapidJSON:BOOL=FALSE
    -DENABLE_INTERNAL_SPDLOG:BOOL=FALSE
    -DENABLE_INTERNAL_TAGLIB:BOOL=FALSE
    -DENABLE_INTERNAL_UDFREAD:BOOL=FALSE
    -DENABLE_ISO9660PP:BOOL=TRUE
    -DENABLE_LCMS2:BOOL=TRUE
    -DENABLE_GOLD:BOOL=FALSE
    -DENABLE_LIRCCLIENT:BOOL=FALSE
    -DENABLE_MARIADBCLIENT:BOOL=FALSE
    -DENABLE_MDNS:BOOL=FALSE
    -DENABLE_MICROHTTPD:BOOL=TRUE
    -DENABLE_OPENGL:BOOL=TRUE
    -DENABLE_OPTICAL:BOOL=TRUE
    -DENABLE_PYTHON:BOOL=TRUE
    -DENABLE_SNDIO:BOOL=FALSE
    -DENABLE_UDEV:BOOL=TRUE
    -DENABLE_UPNP:BOOL=TRUE
    -DENABLE_XSLT:BOOL=TRUE
    -DLIBDVDREAD_URL="${FETCHEDDIR}"/libdvdread-${LIBDVDREAD_REV}.tar.gz
    -DLIBDVDNAV_URL="${FETCHEDDIR}"/libdvdnav-${LIBDVDNAV_REV}.tar.gz
    -DLIBDVDCSS_URL="${FETCHEDDIR}"/libdvdcss-${LIBDVDCSS_REV}.tar.gz
    -DPYTHON_EXECUTABLE:PATH=${PYTHON}
)
CMAKE_SRC_CONFIGURE_OPTION_ENABLES=(
    'airplay AIRTUNES'
    'airplay PLIST'
    'alsa ALSA'
    'avahi AVAHI'
    'bluetooth BLUETOOTH'
    'nfs NFS'
    'pulseaudio PULSEAUDIO'
    'samba SMBCLIENT'
    'sync MYSQLCLIENT'
    'va VAAPI'
    'vdpau VDPAU'
)
CMAKE_SRC_CONFIGURE_TESTS=(
    '-DENABLE_TESTING:BOOL=TRUE -DENABLE_TESTING:BOOL=FALSE'
)

kodi_src_prepare() {
    cmake_src_prepare

    # The FFMPEG find module doesn't use the library names from pkg-config, and it disables the
    # default path search order, which means it won't check the configured CMAKE_FIND_ROOT_PATH.
    # Re-enable the default path search order.
    edo sed -i \
        -e s/NO_DEFAULT_PATH// \
        cmake/modules/FindFFMPEG.cmake
}

kodi_src_configure() {
    local args=()

    args+=( -DPYTHON_INCLUDE_DIR:PATH=$(python_get_incdir) )

    if option wayland; then
        args+=(
            -DCORE_PLATFORM_NAME:STRING=wayland
            -DAPP_RENDER_SYSTEM:STRING=gl
        )
    elif option gbm; then
        args+=(
            -DCORE_PLATFORM_NAME:STRING=gbm
            -DAPP_RENDER_SYSTEM:STRING=gles
        )
    else
        args+=(
            -DCORE_PLATFORM_NAME:STRING=x11
            -DAPP_RENDER_SYSTEM:STRING=gl
        )
    fi

    cmake_src_configure "${args[@]}"
}

kodi_pkg_postinst() {
    freedesktop-desktop_pkg_postinst
    gtk-icon-cache_pkg_postinst
}

kodi_pkg_postrm() {
    freedesktop-desktop_pkg_postrm
    gtk-icon-cache_pkg_postrm
}

