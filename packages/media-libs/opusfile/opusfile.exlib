# Copyright 2014 Konstantinn Bonnet <qu7uux@gmail.com>
# Distributed under the terms of the GNU General Public License v2

SUMMARY="High-level decoding and seeking API for .opus files"
HOMEPAGE="https://opus-codec.org"
DOWNLOADS="https://downloads.xiph.org/releases/opus/${PNV}.tar.gz"

UPSTREAM_DOCUMENTATION="${HOMEPAGE}/docs/${PN}_api-${PV}/index.html"

LICENCES="BSD-3"
SLOT="0"
MYOPTIONS="
    doc
    ( providers: libressl openssl ) [[ number-selected = exactly-one ]]
"

DEPENDENCIES="
    build:
        virtual/pkg-config
        doc? ( app-doc/doxygen[dot] )
    build+run:
        media-libs/libogg[>=1.3]
        media-libs/opus[>=1.0.1]
        providers:libressl? ( dev-libs/libressl:= )
        providers:openssl? ( dev-libs/openssl:= )
"

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --enable-http
    --disable-examples
    --disable-static
)
DEFAULT_SRC_CONFIGURE_OPTION_ENABLES=( doc )

