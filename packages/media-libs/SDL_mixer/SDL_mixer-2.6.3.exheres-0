# Copyright 2009 Maxime Coste <frrrwww@gmail.com>
# Copyright 2010-2015 Wulf C: Krueger <philantrop@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

MY_PNV=${PNV/SDL/SDL2}

require github [ user=libsdl-org project=${PN} release=release-${PV} suffix=tar.gz ]
require cmake
require option-renames [ renames=[ 'modplug mod' 'mpg123 mp3' ] ]

SUMMARY="Multichannel audio mixer library for SDL"
DESCRIPTION="
SDL_mixer is a simple multi-channel audio mixer. It supports 8 channels of 16 bit
stereo audio, plus a single channel of music.
"

LICENCES="ZLIB"
SLOT="2"
PLATFORMS="~amd64 ~x86"
MYOPTIONS="
    flac [[ description = [ Enable FLAC music ] ]]
    midi [[ description = [ Enable MIDI music via timidity ] ]]
    mod [[ description = [ Support loading MOD music with libmodplug ] ]]
    mp3 [[ description = [ Support mp3 files with mpg123 ] ]]
    ogg
    opus [[ description = [ Enable Opus music via opusfile ] ]]
"

DEPENDENCIES="
    build+run:
        media-libs/SDL:2[>=2.0.9]
        flac? ( media-libs/flac:= )
        midi? ( media-sound/timidity++ )
        mod? ( media-libs/libmodplug[>=0.8.8] )
        mp3? ( media-sound/mpg123 )
        ogg? ( media-libs/libvorbis )
        opus? ( media-libs/opusfile[>=0.2] )
"

DEFAULT_SRC_PREPARE_PATCHES+=(
    "${FILES}"/${PN}-Fix-include-files-for-fork.patch
)

src_configure() {
    local cmakeparams=(
        -DBUILD_SHARED_LIBS:BOOL=TRUE
        -DSDL2MIXER_CMD:BOOL=TRUE
        # Link to dependencies instead of dynamically loading them
        -DSDL2MIXER_DEPS_SHARED:BOOL=FALSE
        -DSDL2MIXER_FLAC_DRFLAC:BOOL=FALSE
        -DSDL2MIXER_INSTALL:BOOL=TRUE
        -DSDL2MIXER_MIDI_FLUIDSYNTH:BOOL=FALSE
        # https://github.com/libxmp/libxmp, alternative to libmodplug
        -DSDL2MIXER_MOD_XMP:BOOL=FALSE
        -DSDL2MIXER_MOD_XMP_LITE:BOOL=FALSE
        # Bundled version of https://github.com/mackron/dr_libs, alternative to mpg123
        -DSDL2MIXER_MP3_DRMP3:BOOL=FALSE
        -DSDL2MIXER_SAMPLES:BOOL=FALSE
        -DSDL2MIXER_SAMPLES_INSTALL:BOOL=FALSE
        -DSDL2MIXER_VENDORED:BOOL=FALSE
        -DSDL2MIXER_VORBIS:STRING=$(option ogg VORBISFILE OFF)
        -DSDL2MIXER_VORBIS_STB:BOOL=FALSE
        -DSDL2MIXER_VORBIS_TREMOR:BOOL=FALSE
        -DSDL2MIXER_WAVE:BOOL=TRUE
        $(cmake_option flac SDL2MIXER_FLAC)
        $(cmake_option flac SDL2MIXER_FLAC_LIBFLAC)
        $(cmake_option midi SDL2MIXER_MIDI)
        $(cmake_option midi SDL2MIXER_MIDI_TIMIDITY)
        $(cmake_option mod SDL2MIXER_MOD)
        $(cmake_option mod SDL2MIXER_MOD_MODPLUG)
        $(cmake_option mp3 SDL2MIXER_MP3)
        $(cmake_option mp3 SDL2MIXER_MP3_MPG123)
        $(cmake_option ogg SDL2MIXER_VORBIS_VORBISFILE)
        $(cmake_option opus SDL2MIXER_OPUS)
    )

    ecmake "${cmakeparams[@]}"
}

