# Copyright 2021 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require github [ user=AOMediaCodec tag=v${PV} ] cmake
require alternatives

export_exlib_phases src_install

SUMMARY="A friendly, portable C implementation of the AV1 Image File Format"

LICENCES="BSD-2"
MYOPTIONS="
    apps [[ description = [ Build the avifdec and avifenc applications ] ]]
    sharpyuv [[ description = [ Support \"sharp\" RGB to YUV420 conversion using libsharpyuv (from libwebp) ] ]]
    yuv  [[ description = [ Will use libyuv for conversion from YUV to RGB ] ]]

    ( providers: aom dav1d libgav1 ) [[ number-selected = at-least-one ]]
    ( providers: aom rav1e svt-av1 ) [[ number-selected = at-least-one ]]
    apps? ( ( providers: ijg-jpeg jpeg-turbo ) [[ number-selected = exactly-one ]] )
"

DEPENDENCIES="
    build:
        virtual/pkg-config
    build+run:
        apps? (
            media-libs/libpng:=[>=1.6.32]
            sys-libs/zlib
            providers:ijg-jpeg? ( media-libs/jpeg:= )
            providers:jpeg-turbo? ( media-libs/libjpeg-turbo )
        )
        providers:aom? ( media-libs/aom:= )
        providers:dav1d? ( media-libs/dav1d:= )
        providers:libgav1? ( media-libs/libgav1 )
        providers:rav1e? ( media-video/rav1e )
        providers:svt-av1? ( media-libs/SVT-AV1 )
        sharpyuv? ( media-libs/libwebp:=[>=1.3.0] )
        yuv? ( media-libs/libyuv )
    test:
        dev-cpp/gtest
        media-libs/libpng:=[>=1.6.32]
        sys-libs/zlib
        providers:ijg-jpeg? ( media-libs/jpeg:= )
        providers:jpeg-turbo? ( media-libs/libjpeg-turbo )
    run:
        !media-libs/libavif:0[<0.8.4-r1] [[
            description = [ Alternatives conflict ]
            resolution = upgrade-blocked-before
        ]]
"

CMAKE_SRC_CONFIGURE_PARAMS+=(
    -DBUILD_SHARED_LIBS:BOOL=TRUE
    -DAVIF_BUILD_EXAMPLES:BOOL=FALSE
    # I really like man pages, but pandoc is a bit annoying when it comes
    # to dependencies.
    -DAVIF_BUILD_MAN_PAGES:BOOL=FALSE
    -DAVIF_ENABLE_COVERAGE:BOOL=FALSE
    -DAVIF_ENABLE_WERROR:BOOL=FALSE
)

CMAKE_SRC_CONFIGURE_OPTION_DISABLE_FINDS+=(
    'sharpyuv libsharpyuv'
    'yuv libyuv'
)

CMAKE_SRC_CONFIGURE_OPTIONS+=(
    'apps AVIF_BUILD_APPS'
    # En- and decoder, supports lossless
    'providers:aom AVIF_CODEC_AOM'
    # Decoders
    'providers:dav1d AVIF_CODEC_DAV1D'
    'providers:libgav1 AVIF_CODEC_LIBGAV1'
    # Encoders
    'providers:rav1e AVIF_CODEC_RAV1E'
    'providers:svt-av1 AVIF_CODEC_SVT'
)

CMAKE_SRC_CONFIGURE_TESTS=(
    '-DAVIF_BUILD_TESTS:BOOL=TRUE -DAVIF_BUILD_TESTS:BOOL=FALSE'
    '-DAVIF_ENABLE_GTEST:BOOL=TRUE -DAVIF_ENABLE_GTEST:BOOL=FALSE'
)

if ever at_least 1.0.1 ; then
    CMAKE_SRC_CONFIGURE_PARAMS+=(
        # AVM (AV2) codec is experimental, last checked: 1.0.1
        -DAVIF_CODEC_AVM:BOOL=FALSE
        -DAVIF_ENABLE_EXPERIMENTAL_YCGCO_R:BOOL=FALSE
    )
fi

libavif_src_install() {
    local alternatives=() host=$(exhost --target)

    default

    alternatives+=(
        /usr/${host}/include/avif           avif-${SLOT}
        /usr/${host}/lib/${PN}.so           ${PN}-${SLOT}.so
        /usr/${host}/lib/cmake/${PN}/${PN}-config.cmake         ${PN}-${SLOT}-config.cmake
        /usr/${host}/lib/cmake/${PN}/${PN}-config-none.cmake    ${PN}-${SLOT}-config-none.cmake
        /usr/${host}/lib/cmake/${PN}/${PN}-config-version.cmake ${PN}-${SLOT}-config-version.cmake
        /usr/${host}/lib/pkgconfig/${PN}.pc ${PN}-${SLOT}.pc
    )

    if option apps ; then
        alternatives+=(
            /usr/${host}/bin/avifdec avifdec-${SLOT}
            /usr/${host}/bin/avifenc avifenc-${SLOT}
        )
    fi

    alternatives_for _${host}_${PN} ${SLOT} ${SLOT} "${alternatives[@]}"
}
