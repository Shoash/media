# Copyright 2022-2023 Timo Gurr <tgurr@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require github [ user=mifi project=lossless-cut ]
require freedesktop-desktop

SUMMARY="The swiss army knife of lossless video/audio editing"
DESCRIPTION="
LosslessCut aims to be the ultimate cross platform FFmpeg GUI for extremely fast and lossless
operations on video, audio, subtitle and other related media files. The main feature is lossless
trimming and cutting of video and audio files, which is great for saving space by rough-cutting
your large video files taken from a video camera, GoPro, drone, etc. It lets you quickly extract
the good parts from your videos and discard many gigabytes of data without doing a slow re-encode
and thereby losing quality. Or you can add a music or subtitle track to your video without needing
to encode. Everything is extremely fast because it does an almost direct data copy, fueled by the
awesome FFmpeg which does all the grunt work.
"
DOWNLOADS="
    https://raw.githubusercontent.com/mifi/lossless-cut/v${PV}/no.mifi.losslesscut.desktop -> ${PNV}.desktop
    https://raw.githubusercontent.com/mifi/lossless-cut/v${PV}/src/icon.svg -> ${PNV}.svg
    platform:amd64? ( ${HOMEPAGE}/releases/download/v${PV}/LosslessCut-linux-x64.tar.bz2 -> ${PNV}-linux-x64.tar.bz2 )
"

LICENCES="GPL-2"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS="
    platform: amd64
"

RESTRICT="strip"

DEPENDENCIES="
    run:
        dev-libs/at-spi2-atk
        dev-libs/at-spi2-core
        dev-libs/atk
        dev-libs/expat
        dev-libs/glib:2
        dev-libs/nspr
        dev-libs/nss
        net-print/cups
        sys-apps/dbus
        sys-libs/wayland
        sys-libs/zlib
        sys-sound/alsa-lib
        x11-dri/libdrm
        x11-dri/mesa
        x11-libs/cairo
        x11-libs/gtk+:3
        x11-libs/libX11
        x11-libs/libxcb
        x11-libs/libXcomposite
        x11-libs/libXdamage
        x11-libs/libXext
        x11-libs/libXfixes
        x11-libs/libxkbcommon
        x11-libs/libXrandr
        x11-libs/pango
"

WORK=${WORKBASE}/LosslessCut-linux-x64

pkg_setup() {
    exdirectory --allow /opt
}

src_test() {
    :
}

src_install() {
    insinto /opt/${PN}
    doins -r *

    insinto /usr/share/applications
    newins "${FETCHEDDIR}"/${PNV}.desktop no.mifi.losslesscut.desktop
    edo sed \
        -e 's:Exec=/app/bin/run.sh:Exec=losslesscut %U:g' \
        -i "${IMAGE}"/usr/share/applications/no.mifi.losslesscut.desktop

    insinto /usr/share/icons/hicolor/scalable/apps
    newins "${FETCHEDDIR}"/${PNV}.svg no.mifi.losslesscut.svg

    dodir /usr/$(exhost --target)/bin
    dosym /opt/${PN}/losslesscut /usr/$(exhost --target)/bin/losslesscut

    edo chmod 0755 "${IMAGE}"/opt/${PN}/losslesscut
    edo chmod 0755 "${IMAGE}"/opt/${PN}/resources/{ffmpeg,ffprobe}
}

